import { Box, TextField, Container, Modal, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Pagination, Button, ChildModal, Input } from "@mui/material";
import { useEffect, useState } from "react";
var user = {};
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
  };
function DataTable() {
    const [open, setOpen] = useState(false);
    const handleClose = () => {
      setOpen(false);
    };
    
    const limit = 10;
    const [noPage, setNoPage] = useState(0);
    const [page, setPage] = useState(1);
    const [rows, setRows] = useState([]);
    const fetchAPI = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
    const changePageHandler = (event, value) => {
        setPage(value);
        
       
    }

    const BtnOnClickHander = (row) => {
        console.log(row)
        user = row;
        setOpen(true);
    }
    useEffect(() => {
        fetchAPI("http://42.115.221.44:8080/devcamp-register-java-api/users")
            .then((data) => {
                console.log(data)
                setRows(data.slice((page - 1) * limit, page * limit));
                setNoPage(Math.ceil(data.length / limit));

            })
            .catch((error) => {
                console.log(error.massage);
            })
    }, [page])
    return (

        <Container>
            <h2 align="center"> Danh sách đăng ký</h2>
            <Grid conatiner>
                <Grid item>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <TableCell align="center">id</TableCell>
                                        <TableCell align="center">firstname</TableCell>
                                        <TableCell align="center">lastname</TableCell>
                                        <TableCell align="center">country</TableCell>
                                        <TableCell align="center">subject</TableCell>
                                        <TableCell align="center">customerType</TableCell>
                                        <TableCell align="center">registerStatus</TableCell>
                                        <TableCell align="center">Action</TableCell>
                                    </TableCell>
                                </TableRow>
                                <TableBody>
                                    {
                                        rows.map((row, index) => {
                                            return (
                                                <TableRow>
                                                    <TableCell align="center">{row.id}</TableCell>
                                                    <TableCell align="center">{row.firstname}</TableCell>
                                                    <TableCell align="center">{row.lastname}</TableCell>
                                                    <TableCell align="center">{row.country}</TableCell>
                                                    <TableCell align="center">{row.subject}</TableCell>
                                                    <TableCell align="center">{row.customerType}</TableCell>
                                                    <TableCell align="center">{row.registerStatus}</TableCell>
                                                    <TableCell align="center"><Button variant="outlined" onClick={() => { BtnOnClickHander(row) }}> Chi tiết</Button></TableCell>
                                                </TableRow>
                                            )
                                        })
                                    }
                                </TableBody>
                            </TableHead>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={2} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={page} onChange={changePageHandler} />
                </Grid>
            </Grid>
            <Button >Open modal</Button>
            <Modal
                open={open}
                onClose={open}
                aria-labelledby="parent-modal-title"
                aria-describedby="parent-modal-description"
            >
                <Box sx={{...style, width: 400 }}>
                   <h2>Thông tin user</h2>
                   <TextField label="id" color="secondary" variant="filled" focused  value={user.id} style={{width: '100%'}} mt={3}/>
                   <TextField label="firstname" color="secondary" variant="filled" focused  value={user.firstname} style={{width: '100%'}} mt={3}/>
                   <TextField label="lastname" color="secondary" variant="filled" focused  value={user.lastname} style={{width: '100%'}} mt={3}/>
                   <TextField label="country" color="secondary" variant="filled"  focused  value={user.country} style={{width: '100%'}} mt={3}/>
                   <TextField label="subject" color="secondary" variant="filled" focused  value={user.subject} style={{width: '100%'}} mt={3}/>
                   <TextField label="customerType" color="secondary" variant="filled" focused  value={user.customerType} style={{width: '100%'}} mt={3}/>
                   <TextField label="registerStatus" color="secondary" variant="filled" focused  value={user.registerStatus} style={{width: '100%'}} mt={3}/>
                   <Grid mt={4} align="center">
                    <Button variant="outlined"  onClick={handleClose}>Close</Button>
                   </Grid>

                </Box>
            </Modal>

        </Container>
    )

}
export default DataTable